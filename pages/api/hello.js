// import NextCors from "nextjs-cors";
import connectDB from "../../utils/mongo/dbConnect.js";
import Site from "../../utils/mongo/model.js";
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

const handler = async (req, res) => {
    console.log("/api/hello");
    // await NextCors(req, res, {
    //     // Options
    //     methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
    //     origin: "*",
    //     optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    // });
    if (req.method === "POST") {
        // Check if name, email or password is provided
        const { id } = JSON.parse(req.body);
        console.log("id", id, req.body);
        if (id) {
            try {
                // Hash password to store it in DB
                var existedSite = await Site.findOne({});
                if (!existedSite) {
                    // var newSite = new Site({
                    //     color: "red",
                    // });
                    // existedSite = await newSite.save();
                    // console.log("siteWasCreated");
                    existedSite = { color: "blue" };
                }
                // Create new user
                console.log(existedSite);
                return res.status(200).json(existedSite);
            } catch (error) {
                return res.status(500).json({ error: error.message });
            }
        } else {
            res.status(422).json({ error: "data_incomplete" });
        }
    } else {
        res.status(422).json({ error: "req_method_not_supported" });
    }
};

export default connectDB(handler);
