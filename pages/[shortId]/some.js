export default function webSite({ shortId }) {
    return (
        <div>
            <h1>Some</h1>
            <p>{shortId}</p>
        </div>
    );
}

export async function getServerSideProps(context) {
    console.log(context);
    const { query } = context;
    return { props: { shortId: query.shortId } };
}
/*
// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export async function getStaticProps(context) {
    console.log("getStaticProps", context);
    // const res = await fetch('https://.../posts')
    // const posts = await res.json()

    return {
        props: {},
        // Next.js will attempt to re-generate the page:
        // - When a request comes in
        // - At most once every 10 seconds
        // revalidate: 10, // In seconds
    };
}

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// the path has not been generated.
export async function getStaticPaths() {
    // const res = await fetch("https://.../posts");
    // const posts = await res.json();

    // Get the paths we want to pre-render based on posts
    const paths = [
        {
            params: { shortId: 1 },
        },
    ];

    // We'll pre-render only these paths at build time.
    // { fallback: blocking } will server-render pages
    // on-demand if the path doesn't exist.
    return { paths, fallback: "blocking" };
}
*/
